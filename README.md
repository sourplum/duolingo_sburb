## Duolingo Enhancement Suite!

### Dependencies
* **Duolingo Xp bar** (by th23) https://www.duolingo.com/comment/4914732
* **Duolingo Enhancement Suite Css** (by me) https://userstyles.org/styles/126867/duolingo-enhancement-suite

#### Optional
* **Duolingo Blur Background** (by me) https://userstyles.org/styles/126868/duolingo-blur-background
* **Duolingo Dark Translucence** (by deArt) https://userstyles.org/styles/124013/duolingo-dark-translucence

### Home-Page
[![DUolingo Enhancement Suite in action](http://pix.toile-libre.org/upload/img/1464362212.png)](http://pix.toile-libre.org/upload/original/1464362212.png)
If you click on the small country flag next to the experience bar, you can toggle the stats displayed on top.
The stats are displayed on top.
The EXP bar is merged with the main-page title area, side-panels are removed.

### Discussion Stream
[![alt](http://pix.toile-libre.org/upload/img/1460945766.png)](http://pix.toile-libre.org/upload/original/1460945766.png)
Introducing other tabs:
  * Subscriptions to manage the former
  * \+ (more) to check the incubator

### Immersion
[![alt](http://pix.toile-libre.org/upload/img/1460945775.png)](http://pix.toile-libre.org/upload/original/1460945775.png)
Introducing other tabs:
  * Filter to filter the docs with ease
  * Stats to check your translation tier

### Words
[![alt](http://pix.toile-libre.org/upload/img/1460945968.png)](http://pix.toile-libre.org/upload/original/1460945968.png)
More space

### Gold
[![alt](http://pix.toile-libre.org/upload/img/1460946059.png)](http://pix.toile-libre.org/upload/original/1460946059.png)
Show everyone that you rules!

### Duo is Back
[![alt](http://pix.toile-libre.org/upload/img/1464314599.png)](http://pix.toile-libre.org/upload/original/1464314599.png)
Quit popup when quitting a lesson, theme used **Duolingo Blur Background**

## Updates

**v0.3**:
  * New cool animation system
  * Check out the flags! (click on the flag next to the experience bar)
  * Hidden progression panel by default

**v0.4**:
  * Badge active/locked animation

**v0.5**:
  * Fadein animation on skill-tree
  * lesson test progress bar in blue (will figure out way to customize it later on)

**v0.6**:
  * "Bug fixes and performance improvements." - Duolingo

**v0.7**:
  * Toolbar Color for Duolingo! Works great with Vivaldi Browser, see Android Lollipop Google Chrome theme-color for more details. 

v0.7.2:
  * Golden navbar only for skills of max strengh (5/5)
  * Progress bar in lesson color fixed + update CSS

**v0.8**:
  * Sweet Alert integration
  * Quit a lesson give a nice and modern popup with a little surprise (see screenshots)

**v0.9**:
  * Semantic Ui integration
  * Fixed Stat bar
  * Introducing the Duo streak goal, Duo turns gold when you've achieved your daily goal

v0.9.1:
  * Semantic Ui fixed dependencies, avoid Css overwrite and only the needed parts are downloaded

v0.9.2:
  * Weekly-progress on the left, better view

v0.9.3:
  * Duo is tired of your bullshit, get your strike done already

v0.9.4:
  * Fixed comments not showing during tests
  * Remove color changing (should be in the CSS)
  * Fixed duo dragon not showing (put the src in imgur)
